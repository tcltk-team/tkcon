tkcon (2:2.7.11-3) unstable; urgency=medium

  * No-change to make a source-only upload.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 06 Feb 2025 15:40:15 +0300

tkcon (2:2.7.11-2) unstable; urgency=medium

  * Add a patch from IOhannes m zmölnig, which allows one to use TkCon
    with Tcl/Tk 9.0 via [package require tkcon] (closes: #1093681).
  * /usr/bin/tkcon still uses the default Tcl/Tk (8.6 at the moment).

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 23 Jan 2025 14:07:46 +0300

tkcon (2:2.7.11-1) unstable; urgency=medium

  * New upstream release.
  * Bump the debhelper compatibility level to 13.
  * Bump the standards version to 4.6.2.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 24 Jan 2023 16:28:42 +0300

tkcon (2:2.7.10-2) unstable; urgency=medium

  * Fix the Tcl package version in pkgIndex.tcl and in the installation
    directory.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 26 Aug 2021 15:43:39 +0300

tkcon (2:2.7.10-1) unstable; urgency=medium

  * New upstream release.
  * Fix the debian/watch uscan control file.
  * Bump the standards version to 4.5.1.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 17 Aug 2021 11:32:58 +0300

tkcon (2:2.7.3-1) unstable; urgency=medium

  * New upstream release.
  * Make the binary package multi-arch foreign.
  * Bump the standards version to 4.4.1.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 04 Jan 2020 13:33:01 +0300

tkcon (2:2.7.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the standards version to 4.3.0.
  * Remove the encoding field from and add the keywords field to the desktop
    shortcut file.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 11 Jan 2019 10:18:04 +0300

tkcon (2:2.7.1-1) unstable; urgency=medium

  * New upstream and new upstream version.
  * Remove no longer necessary patch.
  * Comment out embedded external plugin because of possible privacy breach.
  * Bump the debhelper compatibility level to 10.
  * Bump the standards version to 4.1.3.
  * Add the Salsa VCS headers.
  * Add the Tkcon application desktop shortcut.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 23 Mar 2018 22:28:47 +0300

tkcon (2:2.7~20151021-2) unstable; urgency=medium

  * Added a patch which wixes -underline option usage for Tk 8.6.6 and newer.
  * Bumped standards version to 3.9.8.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 09 Aug 2016 16:38:28 +0300

tkcon (2:2.7~20151021-1) unstable; urgency=medium

  * New upstream CVS snapshot with a few bugfixes.
  * Bumped debhelper compatibility level to 9.
  * Bumped standards version to 3.9.7.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 20 Apr 2016 14:25:49 +0300

tkcon (2:2.7~20130123-2) unstable; urgency=low

  * Removed the external Sourceforge logo from the HTML documentaltion.
  * Do not remove macros .BS and .BE from the manpages because they are
    defined now.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 18 May 2014 08:28:09 +0400

tkcon (2:2.7~20130123-1) unstable; urgency=low

  * New upstream CVS snapshot.
  * Switched to 3.0 (quilt) source format.
  * Bumped standards version to 3.9.5.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 29 Jan 2014 08:46:58 +0400

tkcon (2:2.5-1) unstable; urgency=low

  * New upstream release.
  * Added ${misc:Depends} substitution variable to  the debian/control file
    because tkcon is built with debhelper.
  * Fixed debian/watch file.
  * Bumped standards version to 3.9.2.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 23 Aug 2011 20:57:11 +0400

tkcon (1:2.5-20080207-1) unstable; urgency=low

  * New CVS snapshot.
  * Fixed spelling error in description (tcl -> Tcl).
  * Changed doc-base section to Programming.
  * Fixed errors in man (removed undefined .BS and .BE).
  * Removed deprecated linda override file.
  * Bumped standards version to 3.8.0.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 17 Jun 2008 13:35:54 +0400

tkcon (1:2.5-20070623-4) unstable; urgency=low

  * Fixed doc-base registration file (removed extra spaces).
  * Bumped standards version to 3.7.3.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Nov 2007 14:09:17 +0300

tkcon (1:2.5-20070623-3) unstable; urgency=low

  * Adapted the package to Debian Tcl/Tk policy. This includes moving the Tcl
    module into a subdirectory of /usr/share/tcltk and switching to default
    tcl package in dependencies.
  * Added Homepage field in debian/control.
  * Bumped debhelper compatibility level to 5.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Nov 2007 13:56:02 +0300

tkcon (1:2.5-20070623-2) unstable; urgency=low

  * Replaced deprecated Apps section in debian menu file by Applications.
    Also, made menu labels more clear.

 -- Sergei Golovan <sgolovan@debian.org>  Sun, 16 Sep 2007 21:35:51 +0400

tkcon (1:2.5-20070623-1) unstable; urgency=low

  * New CVS snapshot.
  * Removed manual pages from Debian source package since they are included
    into original sources now.
  * Removed patch which fixed overriding widget options since a similar
    changes are added to upstream sources.
  * Removed patch which replaced Windows new line characters by Unix ones
    since they are already fixed in upstream sources.
  * Removed patch which fixed HTML documentation since it is already applied
    to upstream sources.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 15 Aug 2007 14:59:09 +0400

tkcon (1:2.4-20060905-3) unstable; urgency=low

  * New maintainer's email address sgolovan@debian.org.
  * Added build-arch and build-indep targets and fixed build-stamp target in
    debian/rules.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 04 Aug 2007 21:00:40 +0400

tkcon (1:2.4-20060905-2) unstable; urgency=low

  * Fixed overriding widget options when TkCon is used inside a Tcl
    application.

 -- Sergei Golovan <sgolovan@nes.ru>  Wed, 04 Apr 2007 22:30:52 +0400

tkcon (1:2.4-20060905-1) unstable; urgency=low

  * New CVS snapshot (the upstream author confirmed that CVS is stable).
  * Now it's possible to use TkCon as a Tcl package using
    [package require tkcon].
  * Updated upstream author's email address.
  * Added homepage to debian/control.
  * Added get-orig-source target to debian/rules.
  * Fixed Copyright string in debian/copyright.
  * Fixed bug in debian/rules (install target didn't depend on build target).
  * Added exit clause to debian/rules to catch build problems if any.
  * Added debian/watch file.

 -- Sergei Golovan <sgolovan@nes.ru>  Mon, 12 Feb 2007 10:39:25 +0300

tkcon (1:2.4-3) unstable; urgency=low

  * Added registering the package documentation in doc-base.
  * Fixed reference to license.terms in documentation (linked it with
    debian/copyright).
  * Overridden linda warning about extra license file (it's a link, not
    a file).
  * Fixed links in documentation to TkCon' CVS.
  * Use quilt to manage patches.

 -- Sergei Golovan <sgolovan@nes.ru>  Sat, 06 Jan 2007 19:37:22 +0300

tkcon (1:2.4-2) unstable; urgency=low

  * Added manual pages, created from documentation.
  * Changed TkCon menu item title.

 -- Sergei Golovan <sgolovan@nes.ru>  Sat, 06 Jan 2007 16:24:18 +0300

tkcon (1:2.4-1) unstable; urgency=low

  * New maintainer Sergei Golovan (closes: #383658).
  * New upstream version.
  * Added real package to dependency list in addition to virtual package
    wish.

 -- Sergei Golovan <sgolovan@nes.ru>  Fri, 05 Jan 2007 16:34:00 +0300

tkcon (20030408-2) unstable; urgency=low

  * QA Upload
  * Set Maintainer to QA Group, Orphaned: #383658
  * Add missing binary-arch and build target to debian/rules
  * Do not install extra license file.
  * Fix debian/menu
  * Conforms with latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Sat,  2 Sep 2006 12:47:19 +0200

tkcon (20030408-1) unstable; urgency=low

  * Initial Release.
  * (closes: #193002)

 -- David N. Welton <davidw@debian.org>  Mon, 12 May 2003 09:33:34 +0200

